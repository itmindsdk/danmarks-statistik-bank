﻿using ServiceStack.Text;
using StatistikBankAPI.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StatistikBankAPI.Helpers
{
    public class PostMessageBodyBuilder : IPostMessageBodyBuilder
    {
        /// <summary>
        /// A method for building the message body from a FilterVariable
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterVariable">An object describing the filter to apply</param>
        /// <returns>A well formatted JSON string</returns>
        public string BuildMessageBody(string tableId, FilterVariable filterVariable)
        {
            var msgBody = new JsonObject();
            msgBody.Add("table", tableId);
            msgBody.Add("format", "CSV");

            var variablesArray = new JsonArrayObjects();

            var varObj = new JsonObject();
            varObj.Add("code", filterVariable.Key);
            varObj.Add("placement", "Stub");
            varObj.Add("values", $"[\"{filterVariable.Value}\"]");

            variablesArray.Add(varObj);

            msgBody.Add("variables", JsonSerializer.SerializeToString(variablesArray));
            return JsonSerializer.SerializeToString(msgBody);
        }

        /// <summary>
        /// A method for building the message body from a list of FilterVariables
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterVariables">A list of objects describing the filters to apply</param>
        /// <returns>A well formatted JSON string</returns>
        public string BuildMessageBody(string tableId, FilterVariable filterVariable, DataFormat format)
        {
            var msgBody = new JsonObject();
            msgBody.Add("table", tableId);
            msgBody.Add("format", format.ToString());

            var variablesArray = new JsonArrayObjects();

            var varObj = new JsonObject();
            varObj.Add("code", filterVariable.Key);
            varObj.Add("placement", "Stub");
            varObj.Add("values", $"[\"{filterVariable.Value}\"]");

            variablesArray.Add(varObj);

            msgBody.Add("variables", JsonSerializer.SerializeToString(variablesArray));
            return JsonSerializer.SerializeToString(msgBody);
        }

        public string BuildMessageBody(string tableId, List<FilterVariable> filterVariables)
        {
            var msgBody = new JsonObject();
            msgBody.Add("table", tableId);
            msgBody.Add("format", "CSV");

            var variablesArray = new JsonArrayObjects();

            var groupedFilters = filterVariables.GroupBy(fv => fv.Key);
            foreach (var filterGroup in groupedFilters)
            {
                var values = FilterExtensions.Append(filterGroup.ToList());
                var varObj = new JsonObject();
                varObj.Add("code", filterGroup.Key);
                varObj.Add("placement", "Stub");
                varObj.Add("values", values);
                variablesArray.Add(varObj);
            }

            msgBody.Add("variables", JsonSerializer.SerializeToString(variablesArray));
            return JsonSerializer.SerializeToString(msgBody);
        }

        public string BuildMessageBody(string tableId, List<FilterVariable> filterVariables, DataFormat format)
        {
            var msgBody = new JsonObject();
            msgBody.Add("table", tableId);
            msgBody.Add("format", format.ToString());

            var variablesArray = new JsonArrayObjects();

            foreach (var filterByVariable in filterVariables)
            {
                var varObj = new JsonObject();
                varObj.Add("code", filterByVariable.Key);
                varObj.Add("placement", "Stub");
                varObj.Add("values", $"[{filterByVariable.Value}]");

                variablesArray.Add(varObj);
            }

            msgBody.Add("variables", JsonSerializer.SerializeToString(variablesArray));
            return JsonSerializer.SerializeToString(msgBody);
        }
    }
}
