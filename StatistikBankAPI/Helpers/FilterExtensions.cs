﻿using ServiceStack;
using ServiceStack.Text;
using StatistikBankAPI.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StatistikBankAPI.Helpers
{
    public static class FilterExtensions
    {
        #region FilterVariables Logical Operators

        public static string BetweenAndEquals(FilterVariable fromVariable, FilterVariable toVariable)
        {
            return $">={fromVariable.Value}<={toVariable.Value}";
        }

        public static string Between(FilterVariable fromVariable, FilterVariable toVariable)
        {
            return $">{fromVariable.Value}<{toVariable.Value}";
        }

        public static string BetweenAndEquals(string from, string to)
        {
            return $">={from}<={to}";
        }

        public static string Between(string from, string to)
        {
            return $">{from}<{to}";
        }

        public static string BetweenAndEquals(int from, int to)
        {
            return $">={from}<={to}";
        }

        public static string Between(int from, int to)
        {
            return $">{from}<{to}";
        }

        public static string Append(this FilterVariable fromVariable, params FilterVariable[] toVariables)
        {
            var builder = new StringBuilder();
            builder.Append(fromVariable.Value);

            foreach (var variable in toVariables)
            {
                builder.Append($",{variable.Value}");
            }

            return builder.ToString();
        }

        public static string Append(IEnumerable<FilterVariable> variables)
        {
            if(variables == null)
            {
                throw new ArgumentNullException($"{nameof(variables)} was null");
            }

            var builder = new StringBuilder();
            string result = "";
            var variableList = variables.ToList();
            
            if(variableList.Count > 0)
            {
                if (variables.IsNumbers())
                {
                    builder.Append("{\"results\":");
                    builder.Append($"[{variableList[0].Value}");
                    for (int i = 1; i < variableList.Count; i++)
                    {
                        builder.Append($",{variableList[i].Value}");
                    }
                    builder.Append("]}");

                    result = JsonObject.Parse(builder.ToString())
                                                        .GetUnescaped("results");
                }
                else
                {                    
                    builder.Append($"[\"{variableList[0].Value}\"");
                    for (int i = 1; i < variables.Count(); i++)
                    {
                        builder.Append($",\"{variableList[i].Value}\"");
                    }
                    builder.Append("]");

                    result = builder.ToString();
                }
            }
                        

            return result;
        }

        private static bool IsNumbers(this IEnumerable<FilterVariable> filterVariables)
        {
            foreach(var variable in filterVariables)
            {
                int result;
                if(!int.TryParse(variable.Value, out result))
                {
                    return false;
                }
            }
            return true;
        }

        public static IEnumerable<int> ParseListAsNumber(this IEnumerable<FilterVariable> variables)
        {
            if(variables == null)
            {
                throw new ArgumentNullException($"{variables} was null");
            }

            if(variables.Count() > 0)
            {
                foreach(var variable in variables)
                {
                    if(int.TryParse(variable.Value, out int v))
                    {
                        yield return v;
                    }
                }
            }
        }

        #endregion
    }
}
