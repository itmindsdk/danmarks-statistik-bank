﻿using ServiceStack;
using StatistikBankAPI.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StatistikBankAPI.Helpers
{
    public static class DataExtensions
    {
        /// <summary>
        /// Extension method to find a given metadata value from the text property of the variable
        /// </summary>        
        /// <param name="text">should be equivelant to the text property of a Variable</param>
        /// <returns>The id of the first found Variable that contains the supplied text</returns>
        public static string FindIdByText(this TableMetaData metadata, string text)
        {
            var values = metadata.Variables.SelectMany(v => v.values);
            var value = values.FirstOrDefault(v => v.Text.ToLower().Equals(text));
            
            if(value != null)
            {
                return value.Id;
            }

            return null;
        }

        /// <summary>
        /// Extension method for locating all variables within the TableMetaData object that has the supplied text value
        /// </summary>        
        /// <param name="text">should be equivelant to the text property of a Variable</param>
        /// <returns></returns>
        public static IEnumerable<Variable> FindMetaDataVariableValues(this TableMetaData metadata, string text)
        {
            return metadata.Variables.Where(mv => mv.Text.ToLower().Equals(text)).SelectMany(v => v.values);            
        }

        
        /// <summary>
        /// Extension method for extracting, and converting all filterable values on a given TableMetaData object
        /// </summary>        
        /// <returns>An IEnumerable of FilterVariables</returns>
        public static IEnumerable<FilterVariable> GetFilterVariables(this TableMetaData metaData)
        {
            var filters = new List<FilterVariable>();

            foreach(var variable in metaData.Variables)
            {
                foreach (var value in variable.values)
                {
                    filters.Add(new FilterVariable
                    {
                        Key = variable.Id,
                        Value = value.Id
                    });
                }

            }

            return filters;
        }
    }
}
