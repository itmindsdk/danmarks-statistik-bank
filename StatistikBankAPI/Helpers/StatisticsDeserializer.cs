﻿using ServiceStack;
using ServiceStack.Text;
using ServiceStack.Text.Json;
using StatistikBankAPI.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StatistikBankAPI.Helpers
{
    public class StatisticsDeserializer
    {
        public static Dataset Deserialize(string csvString)
        {
            var lines = csvString.FromCsv<List<string>>();            
            
            var data = new Dataset();
            data.ColumnNames = lines.ElementAt(0).Split(';').ToList();

            lines.RemoveAt(0);

            foreach (var line in lines)
            {
                var fields = line.Split(';').ToList();
                var value = fields.ElementAt(fields.Count - 1);
                fields.RemoveAt(fields.Count - 1);
                data.DataDictionary.Add(string.Join(":", fields), value);
            }

            return data;
        }

    }
}
