﻿using StatistikBankAPI.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace StatistikBankAPI.Helpers
{
    public interface IPostMessageBodyBuilder
    {        
        string BuildMessageBody(string tableId, FilterVariable filterVariable);        
        string BuildMessageBody(string tableId, List<FilterVariable> filterVariables);
    }
}
