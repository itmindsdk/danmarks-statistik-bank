﻿using ServiceStack.Text;
using StatistikBankAPI.DTOs;
using StatistikBankAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StatistikBankAPI
{
    public class StatisticsClient : IStatisticsClient
    {
        private static HttpClient Client = new HttpClient();
        private static readonly string BaseEndPoint = "https://api.statbank.dk/v1/";
        private readonly string Language;

        public StatisticsClient()
        {
            this.Language = "da";
        }

        public StatisticsClient(Language language)
        {
            this.Language = language.ToString().ToLower();                       
        }

        static StatisticsClient()
        {
            var url = new Uri(BaseEndPoint);
            Client.BaseAddress = url;
            ServicePointManager.FindServicePoint(url).ConnectionLeaseTimeout = 60 * 1000;
        }

        #region Subjects
        /// <summary>
        /// An Asynchronous method for retrieving all the subjects from the API
        /// </summary>
        /// <param name="rescursive">When set to true the API will be queried recursively, adding child subjects if there are any. The default value is false</param>
        /// <param name="includeTables">When set to true the subjects will include the table information, if table info is present</param>
        /// <param name="omitSubjectsWithoutTables">When set to true subjects without tables will be omitted</param>
        /// <returns>An IEnumerable containing the Subjects</returns>
        public async Task<IEnumerable<Subject>> GetAllSubjectsAsync(bool rescursive = false, bool includeTables = false, bool omitSubjectsWithoutTables = false)
        {
            var result = await Client.GetAsync($"subjects?lang={Language}&format=JSON&recursive={rescursive}&includetables={includeTables}");

            if(result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return JsonSerializer.DeserializeFromString<IEnumerable<Subject>>(data);
                }
                catch(Exception ex)
                {
                    throw new Exception($"{nameof(GetAllSubjectsAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An Asynchronous method for retrieving subjects based on ids
        /// </summary>
        /// <param name="subjectIds">An IEnumerable containing the ids for the subjects to retrieve</param>
        /// <param name="rescursive">When set to true the API will be queried recursively, adding child subjects if there are any. The default value is false</param>
        /// <param name="includeTables">>When set to true the subjects will include the table information, if table info is present</param>
        /// <param name="omitSubjectsWithoutTables">When set to true subjects without tables will be omitted</param>
        /// <returns>An IEnumerable containing the Subjects</returns>
        public async Task<IEnumerable<Subject>> GetSubjectsAsync(IEnumerable<string> subjectIds, bool rescursive = false, bool includeTables = false, bool omitSubjectsWithoutTables = false)
        {
            var idListAsString = string.Join(",", subjectIds.ToList().Select(id => id));
            
            var result = await Client.GetAsync($"subjects/{idListAsString}?lang={Language}&format=JSON&recursive={rescursive}&includetables={includeTables}");

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return JsonSerializer.DeserializeFromString<IEnumerable<Subject>>(data);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetSubjectsAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }
        #endregion Subjects

        #region Tables

        /// <summary>
        /// An asynchronous method for retrieving all the tables from the API
        /// </summary>
        /// <param name="includeInactive">When set to true all tables, including inactive tables will be returned</param>
        /// <returns>An IEnumerable containing the tables</returns>
        public async Task<IEnumerable<Table>> GetAllTablesAsync(bool includeInactive = false)
        {
            var result = await Client.GetAsync($"tables?lang={Language}&format=JSON&includeinactive={includeInactive}");

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return JsonSerializer.DeserializeFromString<IEnumerable<Table>>(data);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetAllTablesAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An asynchronous method for retrieving all the tables from the API
        /// </summary>
        /// <param name="pastDays">Only include tables that have been updated within the number of days specified</param>
        /// <param name="includeInactive">When set to true all tables, including inactive tables will be returned</param>
        /// <returns>An IEnumerable containing the tables</returns>
        public async Task<IEnumerable<Table>> GetAllTablesAsync(int pastDays, bool includeInactive = false)
        {
            var result = await Client.GetAsync($"tables?lang={Language}&format=JSON&pastDays={pastDays}&includeinactive{includeInactive}");

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return JsonSerializer.DeserializeFromString<IEnumerable<Table>>(data);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetAllTablesAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An asynchronous method for retrieving tables, based on related Subjects
        /// </summary>
        /// <param name="subjectIds">An IEnumerable containing the ids of the related subjects</param>
        /// <param name="includeInactive">When set to true all tables, including inactive tables will be returned</param>
        /// <returns>An IEnumerable containing the tables</returns>
        public async Task<IEnumerable<Table>> GetTablesBySubjectAsync(IEnumerable<string> subjectIds, bool includeInactive = false)
        {
            var idListAsString = string.Join(",", subjectIds.ToList().Select(id => id));

            var result = await Client.GetAsync($"tables?lang={Language}&format=JSON&subjects={idListAsString}&includeinactive={includeInactive}");

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return JsonSerializer.DeserializeFromString<IEnumerable<Table>>(data);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetTablesBySubjectAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An asynchronous method for retrieving tables, based on related Subjects
        /// </summary>
        /// <param name="subjectIds">An IEnumerable containing the ids of the related subjects</param>
        /// <param name="pastDays">Only include tables that have been updated within the number of days specified</param>
        /// <param name="includeInactive">When set to true all tables, including inactive tables will be returned</param>
        /// <returns>An IEnumerable containing the tables</returns>
        public async Task<IEnumerable<Table>> GetTablesBySubjectAsync(IEnumerable<string> subjectIds, int pastDays, bool includeInactive = false)
        {
            var idListAsString = string.Join(",", subjectIds.ToList().Select(id => id));

            var result = await Client.GetAsync($"tables?lang={Language}&format=JSON&subjects={idListAsString}&pastdays={pastDays}&includeinactive={false}");

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return JsonSerializer.DeserializeFromString<IEnumerable<Table>>(data);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetTablesBySubjectAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }
        #endregion Tables

        #region TableMetaData

        /// <summary>
        /// An asynchronous method for retrieving table metadata
        /// </summary>
        /// <param name="tableId">The id of the table to recieve metadata from</param>
        /// <returns>A TableMetaData object containing the metadata</returns>
        public async Task<TableMetaData> GetTableMetaDataAsync(string tableId)
        {
            var result = await Client.GetAsync($"tableinfo/{tableId}?lang={Language}&format=JSON");

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return JsonSerializer.DeserializeFromString<TableMetaData>(data);
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetAllTablesAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        #endregion TableMetaData

        #region TableData

        /// <summary>
        /// An asynchronous method for retrieving table data
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="dataFormat">The format the result should be returned in. The default is JSONSTAT</param>
        /// <returns>A string representation of the formatted data</returns>
        public async Task<string> GetTableDataAsync(string tableId, DataFormat dataFormat = DataFormat.JSONSTAT)
        {
            var result = await Client.GetAsync($"data/{tableId}/{dataFormat.ToString()}?lang={Language}&valuePresentation=CodeAndValue");

            if (result.IsSuccessStatusCode)
            {
                if(dataFormat != DataFormat.PX)
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return data;
                }
                else
                {
                    var data = await result.Content.ReadAsByteArrayAsync();
                    return Encoding.Default.GetString(data);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An asynchronous method for retrieving table data
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <returns>A Dataset containing the data for the specified table</returns>
        public async Task<Dataset> GetTableDataAsync(string tableId)
        {
            var result = await Client.GetAsync($"data/{tableId}/CSV?lang={Language}");

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    var tableData = StatisticsDeserializer.Deserialize(data);
                    return tableData;
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetTableDataAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An asynchronous method for retrieving table data filtered by the filtervariable specified
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterByVariable">A variable object containing the filter information</param>
        /// <returns>A Dataset containing the data for the specified table</returns>
        public async Task<Dataset> GetTableDataAsync(string tableId, FilterVariable filterByVariable)
        {
            var messageBodyBuilder = new PostMessageBodyBuilder();

            var result = await Client.PostAsync($"data?lang={Language}", new StringContent(messageBodyBuilder.BuildMessageBody(tableId, filterByVariable), Encoding.UTF8, "application/json"));

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    
                    var tableData = StatisticsDeserializer.Deserialize(data);
                    return tableData;
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetTableDataAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An asynchronous method for retrieving table data filtered by the filtervariable specified
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterByVariable">A variable object containing the filter information</param>
        /// <param name="dataFormat">The format the result should be returned in. The default is JSONSTAT</param>
        /// <returns>A string representation of the formatted data</returns>
        public async Task<string> GetTableDataAsync(string tableId, FilterVariable filterByVariable, DataFormat dataFormat = DataFormat.JSONSTAT)
        {
            var messageBodyBuilder = new PostMessageBodyBuilder();
            
            var result = await Client.PostAsync($"data?lang={Language}", new StringContent(messageBodyBuilder.BuildMessageBody(tableId, filterByVariable, dataFormat), Encoding.UTF8, "application/json"));

            if (result.IsSuccessStatusCode)
            {
                if (dataFormat != DataFormat.PX)
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return data;
                }
                else
                {
                    var data = await result.Content.ReadAsByteArrayAsync();
                    return Encoding.Default.GetString(data);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An asynchronous method for retrieving table data filtered by a list of filtervariables
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterByVariables">A list of variable objects containing the filter information</param>
        /// <returns>A Dataset object containing the table data</returns>
        public async Task<Dataset> GetTableDataAsync(string tableId, List<FilterVariable> filterByVariables)
        {
            var messageBodyBuilder = new PostMessageBodyBuilder();
            var msg = messageBodyBuilder.BuildMessageBody(tableId, filterByVariables);
            var result = await Client.PostAsync($"data?lang={Language}", new StringContent(msg, Encoding.UTF8, "application/json"));

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    var data = await result.Content.ReadAsStringAsync();
                    
                    var tableData = StatisticsDeserializer.Deserialize(data);
                    return tableData;
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(GetTableDataAsync)} failed to deserialize the data recieved. See inner exception for details.", ex);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }

        /// <summary>
        /// An asynchronous method for retrieving table data filtered by a list of filtervariables
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterByVariables">A list of variable objects containing the filter information</param>
        /// <param name="dataFormat">The format the result should be returned in. The default is JSONSTAT</param>
        /// <returns>A string representation of the formatted data</returns>
        public async Task<string> GetTableDataAsync(string tableId, List<FilterVariable> filterByVariables, DataFormat dataFormat = DataFormat.JSONSTAT)
        {
            var messageBodyBuilder = new PostMessageBodyBuilder();

            var result = await Client.PostAsync($"data?lang={Language}", new StringContent(messageBodyBuilder.BuildMessageBody(tableId, filterByVariables, dataFormat), Encoding.UTF8, "application/json"));

            if (result.IsSuccessStatusCode)
            {
                if (dataFormat != DataFormat.PX)
                {
                    var data = await result.Content.ReadAsStringAsync();
                    return data;
                }
                else
                {
                    var data = await result.Content.ReadAsByteArrayAsync();
                    return Encoding.Default.GetString(data);
                }
            }
            else
            {
                throw new Exception($"The request failed with statuscode: {result.StatusCode}, the response recieved was:\n {result.Content.ReadAsStringAsync().Result}");
            }
        }       

        #endregion TableData
    }
}
