﻿namespace StatistikBankAPI
{
    /// <summary>
    /// Represents the desired format used when getting tabledata
    /// </summary>
    public enum DataFormat
    {
        CSV = 1,
        JSONSTAT = 2,
        PX = 3,
        DSTML = 4
    }
}
