﻿using StatistikBankAPI.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StatistikBankAPI
{
    public interface IStatisticsClient
    {
        /// <summary>
        /// An Asynchronous method for retrieving all the subjects from the API
        /// </summary>
        /// <param name="rescursive">When set to true the API will be queried recursively, adding child subjects if there are any. The default value is false</param>
        /// <param name="includeTables">When set to true the subjects will include the table information, if table info is present</param>
        /// <param name="omitSubjectsWithoutTables">When set to true subjects without tables will be omitted</param>
        /// <returns>An IEnumerable containing the Subjects</returns>
        Task<IEnumerable<Subject>> GetAllSubjectsAsync(bool rescursive = false, bool includeTables = false, bool omitSubjectsWithoutTables = false);

        /// <summary>
        /// An Asynchronous method for retrieving subjects based on ids
        /// </summary>
        /// <param name="subjectIds">An IEnumerable containing the ids for the subjects to retrieve</param>
        /// <param name="rescursive">When set to true the API will be queried recursively, adding child subjects if there are any. The default value is false</param>
        /// <param name="includeTables">>When set to true the subjects will include the table information, if table info is present</param>
        /// <param name="omitSubjectsWithoutTables">When set to true subjects without tables will be omitted</param>
        /// <returns>An IEnumerable containing the Subjects</returns>
        Task<IEnumerable<Subject>> GetSubjectsAsync(IEnumerable<string> subjectIds, bool rescursive = false, bool includeTables = false, bool omitSubjectsWithoutTables = false);

        /// <summary>
        /// An asynchronous method for retrieving all the tables from the API
        /// </summary>
        /// <param name="includeInactive">When set to true all tables, including inactive tables will be returned</param>
        /// <returns>An IEnumerable containing the tables</returns>
        Task<IEnumerable<Table>> GetAllTablesAsync(bool includeInactive = false);

        /// <summary>
        /// An asynchronous method for retrieving all the tables from the API
        /// </summary>
        /// <param name="pastDays">Only include tables that have been updated within the number of days specified</param>
        /// <param name="includeInactive">When set to true all tables, including inactive tables will be returned</param>
        /// <returns>An IEnumerable containing the tables</returns>
        Task<IEnumerable<Table>> GetAllTablesAsync(int pastDays, bool includeInactive = false);

        /// <summary>
        /// An asynchronous method for retrieving tables, based on related Subjects
        /// </summary>
        /// <param name="subjectIds">An IEnumerable containing the ids of the related subjects</param>
        /// <param name="includeInactive">When set to true all tables, including inactive tables will be returned</param>
        /// <returns>An IEnumerable containing the tables</returns>
        Task<IEnumerable<Table>> GetTablesBySubjectAsync(IEnumerable<string> subjectIds, bool includeInactive = false);

        /// <summary>
        /// An asynchronous method for retrieving tables, based on related Subjects
        /// </summary>
        /// <param name="subjectIds">An IEnumerable containing the ids of the related subjects</param>
        /// <param name="pastDays">Only include tables that have been updated within the number of days specified</param>
        /// <param name="includeInactive">When set to true all tables, including inactive tables will be returned</param>
        /// <returns>An IEnumerable containing the tables</returns>
        Task<IEnumerable<Table>> GetTablesBySubjectAsync(IEnumerable<string> subjectIds, int pastDays, bool includeInactive = false);

        /// <summary>
        /// An asynchronous method for retrieving table metadata
        /// </summary>
        /// <param name="tableId">The id of the table to recieve metadata from</param>
        /// <returns>A TableMetaData object containing the metadata</returns>
        Task<TableMetaData> GetTableMetaDataAsync(string tableId);

        /// <summary>
        /// An asynchronous method for retrieving table data
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <returns>A Dataset containing the data for the specified table</returns>
        Task<Dataset> GetTableDataAsync(string tableId);

        /// <summary>
        /// An asynchronous method for retrieving table data
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="dataFormat">The format the result should be returned in. The default is JSONSTAT</param>
        /// <returns>A string representation of the formatted data</returns>
        Task<string> GetTableDataAsync(string tableId, DataFormat format);

        /// <summary>
        /// An asynchronous method for retrieving table data filtered by the filtervariable specified
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterByVariable">A variable object containing the filter information</param>
        /// <returns>A Dataset containing the data for the specified table</returns>
        Task<Dataset> GetTableDataAsync(string tableId, FilterVariable filterByVariable);

        /// <summary>
        /// An asynchronous method for retrieving table data filtered by the filtervariable specified
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterByVariable">A variable object containing the filter information</param>
        /// <param name="dataFormat">The format the result should be returned in. The default is JSONSTAT</param>
        /// <returns>A string representation of the formatted data</returns>
        Task<string> GetTableDataAsync(string tableId, FilterVariable filterByVariable, DataFormat format);

        /// <summary>
        /// An asynchronous method for retrieving table data filtered by a list of filtervariables
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterByVariables">A list of variable objects containing the filter information</param>
        /// <returns>A Dataset object containing the table data</returns>
        Task<Dataset> GetTableDataAsync(string tableId, List<FilterVariable> filterByVariables);

        /// <summary>
        /// An asynchronous method for retrieving table data filtered by a list of filtervariables
        /// </summary>
        /// <param name="tableId">The id of the table to query</param>
        /// <param name="filterByVariables">A list of variable objects containing the filter information</param>
        /// <param name="dataFormat">The format the result should be returned in. The default is JSONSTAT</param>
        /// <returns>A string representation of the formatted data</returns>
        Task<string> GetTableDataAsync(string tableId, List<FilterVariable> filterByVariables, DataFormat format);

    }
}
