﻿namespace StatistikBankAPI.DTOs
{
    /// <summary>
    /// Represents a variable. a variable can contain information about the values of a given MetadataVariable
    /// </summary>
    public class Variable
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }
}