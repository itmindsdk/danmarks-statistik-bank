﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatistikBankAPI.DTOs
{
    /// <summary>
    /// Represents a table. Contains information about a given table
    /// </summary>
    public class Table
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Unit { get; set; }
        public DateTime Updated { get; set; }
        public string FirstPeriod { get; set; }
        public string LatestPeriod { get; set; }
        public bool Active { get; set; }
        public List<string> Variables { get; set; }
    }
}
