﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatistikBankAPI.DTOs
{
    /// <summary>
    /// represents a filterable variable in the API e.g Key = "alder", Value = "12"
    /// </summary>
    public class FilterVariable
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
