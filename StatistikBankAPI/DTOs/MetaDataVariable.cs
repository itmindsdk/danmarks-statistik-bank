﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatistikBankAPI.DTOs
{
    /// <summary>
    /// Represents a meta data variable containing the values of the variable
    /// </summary>
    public class MetaDataVariable
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public bool Elimination { get; set; }
        public bool Time { get; set; }
        public IEnumerable<Variable> values { get; set; }
    }
}
