﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatistikBankAPI.DTOs
{
    public class Dataset
    {        
        public Dictionary<string,string> DataDictionary { get; set; }
        public List<string> ColumnNames { get; set; }
        public Dataset()
        {
            DataDictionary = new Dictionary<string, string>();
            ColumnNames = new List<string>();
        }
    }
}
