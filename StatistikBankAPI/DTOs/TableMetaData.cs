﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatistikBankAPI.DTOs
{
    /// <summary>
    /// Represents the metadata for a given table
    /// </summary>
    public class TableMetaData
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public string SuppressedDataValue { get; set; }
        public DateTime Updated { get; set; }
        public bool Active { get; set; }
        public List<MetaDataVariable> Variables { get; set; }
    }
}
