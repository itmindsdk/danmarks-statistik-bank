﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatistikBankAPI.DTOs
{
    /// <summary>
    /// Represents a subject from the API, with child subjects and table info
    /// </summary>
    public class Subject
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool HasSubjects { get; set; }
        public IEnumerable<Subject> Subjects { get; set; }
        public IEnumerable<Table> Tables { get; set; }     
    }
}
