﻿using StatistikBankAPI;
using StatistikBankAPI.DTOs;
using StatistikBankAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StatisticsExample
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //Create instance of StatisticsClient
            var client = new StatisticsClient();

            //Retrieve all subjects
            var subjects = await client.GetAllSubjectsAsync();

            foreach(var subject in subjects)
            {
                Console.WriteLine($"Subject Id: {subject.Id} Description: {subject.Description}");
            }


            //Extracting the first and middle subject id from the subjectIds enumerable
            var subjectIds = subjects.Select(sub => sub.Id);
            var subjectFirst = subjectIds.ElementAt(0);
            var subjectMiddle = subjectIds.ElementAt(subjectIds.Count() / 2);

            //Instantiating a list with the subject ids
            var ids = new List<string>
            {
                subjectFirst, subjectMiddle
            };

            //Retrieving tables, based on the ids list
            var tables = await client.GetTablesBySubjectAsync(ids);


            //Extracting the first and middle table id from the tableIds enumerable
            var tableIds = tables.Select(t => t.Id);
            var tableFirst = tableIds.ElementAt(0);
            var tableMiddle = tableIds.ElementAt(tableIds.Count() / 2);


            //Retrieving tableMetaData, based on the first, and middle table extracted
            var tableFirstMetaData = await client.GetTableMetaDataAsync(tableFirst);
            var tableMiddleMetaData = await client.GetTableMetaDataAsync(tableMiddle);

            //Retrieving all possible filter values from the metadata
            var firstFilters = tableFirstMetaData.GetFilterVariables();
            var middleFilters = tableMiddleMetaData.GetFilterVariables();

            //locating the time dimension filter values (Tid = time)
            var timefilter = firstFilters.Where(f => f.Key.Equals("Tid"));

            //Retrieving the first and middle quarter values from the time dimension
            var firstQuarter = timefilter.First();
            var middleQuarter = timefilter.ElementAt(timefilter.Count() / 2);
            
            //Retrieving table data from the first quarter
            var firstTableData = await client.GetTableDataAsync(tableFirst, firstQuarter);
            
            foreach(var set in firstTableData.DataDictionary)
            {
                Console.WriteLine($"Quarter: {set.Key}, Value: {set.Value}");
            }

            // Creating custom expression filter from previously retrieved filter variables
            //var expressionFilter = new FilterVariable
            //{
            //    Key = firstQuarter.Key,
            //    Value = FilterExtensions.Between(firstQuarter, middleQuarter)
            //};

            var area1 = new FilterVariable
            {
                Key = "OMRÅDE",
                Value = "101"
            };

            var area2 = new FilterVariable
            {
                Key = "OMRÅDE",
                Value = "147"
            };

            var area3 = new FilterVariable
            {
                Key = "OMRÅDE",
                Value = "671"
            };

            var area4 = new FilterVariable
            {
                Key = "OMRÅDE",
                Value = "360"
            };

            var area5 = new FilterVariable
            {
                Key = "OMRÅDE",
                Value = "330"
            };

            var age = new FilterVariable
            {
                Key = "ALDER",
                Value = FilterExtensions.BetweenAndEquals(6, 20)
            };

            var time1 = new FilterVariable
            {
                Key = "Tid",
                Value = "2010K1"
            };

            var time2 = new FilterVariable
            {
                Key = "Tid",
                Value = "2018K1"
            };

            var time3 = new FilterVariable
            {
                Key = "Tid",
                Value = "2008K1"
            };

            var time4 = new FilterVariable
            {
                Key = "Tid",
                Value = "2010K1"
            };

            var firstTableDataMultipleQuarters = await client.GetTableDataAsync(tableFirst, new List<FilterVariable> { area1, area2, area3, area4, area5, time1, age, time2, time3, time4 });

            foreach (var set in firstTableDataMultipleQuarters.DataDictionary)
            {
                Console.WriteLine($"Key: {set.Key} Value: {set.Value}");
            }

            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }
    }
}
